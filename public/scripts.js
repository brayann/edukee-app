$(document).ready(function() {
	
	$(".checkout").click(function(event) {

		var id = $(this).parents('tr').attr('id');
		
		modal = $("#checkout");
		
		$.get('/api/produto/' + id, function(data, textStatus, xhr) {
			
			modal.find("#myModalLabel").append(data.nome);
			modal.find(".modal-body #descricao").append(data.descricao);
			modal.find(".modal-body #valor").append(data.valor);

			modal.find("#input_descricao").val(data.nome);
			modal.find("#input_valor").val(data.valor);

			modal.find("#id_produto").val(data.id);

		});

	});

});