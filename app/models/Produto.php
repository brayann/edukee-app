<?php

use LaravelBook\Ardent\Ardent;

class Produto extends Ardent implements ProdutoRepositoryInterface {

	use TraitBase;

	//produtos.
	protected $table = 'produto';

	protected $guarded = array('id');
	//protected $fillable = array('nome');
	//
	protected $softDelete = true;

	public static $rules = array(
		'nome'	=> 'required|unique:produto',
		'valor'	=> 'required',
		'descricao'	=> 'required'
	);

	public function scopeByPrice($query, $valor)
    {
        return $query->where('valor', '>=', $valor);
    }

    public function categoria() {

    	return $this->belongsTo('Categoria', 'categorias_id');

    }

}