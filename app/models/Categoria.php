<?php

class Categoria extends Eloquent {
	
	protected $guarded = array('id');

	public function produtos() {

		return $this->hasMany('Produto', 'categorias_id');

	}

}