<?php

class Venda extends Eloquent {
	
	protected $guarded = array('id');
	protected $table = 'venda';


	public function itens () {

		return $this->belongsToMany('Produto', 'venda_item')->withPivot('qtde');

	}

}