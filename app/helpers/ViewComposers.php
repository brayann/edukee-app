<?php

class ViewComposers {

	public static function register() {

		View::composer('produtos', function($view)
		{
			$produtos = Cache::remember('produtos', 10, function()
			{
		    	return Produto::with('categoria')->get();
			});

			$queries = DB::getQueryLog();
			//dd($queries);

		    $view->with('produtos', $produtos);
		});

		View::composer('admin.produto.create', function($view)
		{
			$categorias = static::getCategorias();
			$dadosCategoria = array();

			foreach ($categorias as $categoria) {
				$dadosCategoria[$categoria->id] = $categoria->nome;
			}

		    $view->with('categorias', $dadosCategoria);
		});

		View::composer('elements.header', function($view)
		{

			$categorias = static::getCategorias();

		    $view->with('categorias', $categorias);
		});

	}

	private static function getCategorias() {
Cache::forget('categorias');
		return Cache::remember('categorias', 60, function()
		{
	    	return Categoria::all();
		});

	}

}
