<?php

class ProdutoEventHandler {
	
	public function limpaCache($data) {

		Cache::forget('produtos');

	}

	public function subscribe($evento) {

		$evento->listen("produtos.alterados", 'ProdutoEventHandler@limpaCache');

	}

}