<?php namespace Admin;

class BaseController extends \Controller {

	protected $layout = 'layouts.admin';

	public function __construct() {

		\Asset::add('estilo', 'styles.css', 'produtos');
		\Asset::add('scripts', 'script.js');

	}

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = \View::make($this->layout);
		}
	}

}