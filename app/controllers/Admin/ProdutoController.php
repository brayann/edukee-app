<?php namespace Admin;

class ProdutoController extends BaseController {

	private $produto;

	public function __construct(\ProdutoRepositoryInterface $p) {

		parent::__construct();
		$this->produto = $p;

		$this->beforeFilter('csrf', array('on' => 'post'));

	}

	function getIndex() {

		\Asset::add('produtos', 'produtos.css');

		$this->layout->content = \View::make('produtos');
		
	}

	function getCreate() {

		$this->layout->content = \View::make('admin.produto.create');

	}

	public function getEditar($id) {

		$produto = $this->produto->find($id);

		echo $this->produto->edukee();

		\Log::info('Log Produto ' . $id);

		if (!$produto) {

			throw new \InvalidProductException("Produto $id Não encontrado");

		}

		$this->layout->content = \View::make('admin.produto.create')->with('produto', $produto);

	}

	public function postEditar() {

		$produto = \Produto::find(\Input::get('id'));

		if (!$produto) {

			return \Redirect::to('admin/produto');

		}

		$dados = \Input::all();
		unset($dados['id']);

		$produto->fill($dados);

		$produto->save();
		\Event::fire('produtos.alterados', array($produto) );
		\Session::flash('sucesso', 'Produto alterado com sucesso');

		return \Redirect::to('admin/produto');

	}

	public function getView($id) {


		$produto = \Produto::find($id);
		//\Cache::section('produtos')->put($id, $produto, 5);
		//$produto = \Cache::section('produtos')->get($id);

		$this->layout->content = \View::make('admin.produto.visualizar')->with('produto', $produto);


	}

	public function getExcluir($id) {

		$produto = \Produto::find($id);

		if (!$produto) {

			return \Redirect::to('admin/produto');

		}

		$produto->delete();

		return \Redirect::to('admin/produto');

	}

	function postCreate() {

		try {

			$produto = new \Produto(\Input::all());

			if (!$produto->save()) {
				throw new \Exception("Falha na validação");
			}

			\Session::flash('sucesso', 'Produto cadastrado com sucesso');

			\Event::fire('produtos.alterados', array($produto) );

		} catch(\Exception $e) {
			return \Redirect::to('admin/produto/create')->withInput()->withErrors($produto->errors());
		}

		return \Redirect::to('admin/produto');

	}

}