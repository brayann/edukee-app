<?php

class HomeController extends BaseController {

	function __construct() {


		$this->beforeFilter('csrf', array('on' => 'post'));
		parent::__construct();

	}

	public function getProdutos() {
		Asset::add('script', 'scripts.js'); 
		$this->layout->content = View::make('produtos');

	}

	public function getIndex() {

		return View::make('index');
	}

	public function getVenda() {

		$venda = Venda::find(1);

		return View::make('venda')->with('venda', $venda);
 
	}

	public function getCategoria() {

		$categoria = Categoria::find(1);
		$categoria->produtos()->where('valor', '>', 2000)->get();

		foreach($categoria->produtos()->where('valor', '>', 2000)->get() as $produto) {
			print_r($produto);
		}

	}

	public function getVendas() {

		$vendas = DB::table('venda')
				  ->join('venda_item', 'venda.id', '=', 'venda_item.venda_id')
				  ->join('produto', 'venda_item.produto_id', '=', 'produto.id')
				  ->get();

		dd($vendas);

	}

	public function getLogin() {

		$this->layout->content = View::make('login');

	}

	public function postLogin() {

		$data = Input::all();
		unset($data['_token']);

		if (Auth::attempt($data)) {

			return Redirect::to('admin/produto');

		} else {

			return Redirect::to('login')->withInput();

		}

	}

	public function getLogout () {

		Auth::logout();
		return Redirect::to("/");

	}

}