<?php

class TestesController extends BaseController {

    /**
     * Testis Repository
     *
     * @var Testis
     */
    protected $testis;

    public function __construct(Teste $testis)
    {
        $this->testis = $testis;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $testes = $this->testis->all();

        return View::make('testes.index', compact('testes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('testes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $validation = Validator::make($input, Teste::$rules);

        if ($validation->passes())
        {
            $this->testis->create($input);

            return Redirect::route('testes.index');
        }

        return Redirect::route('testes.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $testis = $this->testis->findOrFail($id);

        return View::make('testes.show', compact('testis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $testis = $this->testis->find($id);

        if (is_null($testis))
        {
            return Redirect::route('testes.index');
        }

        return View::make('testes.edit', compact('testis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Testis::$rules);

        if ($validation->passes())
        {
            $testis = $this->testis->find($id);
            $testis->update($input);

            return Redirect::route('testes.show', $id);
        }

        return Redirect::route('testes.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->testis->find($id)->delete();

        return Redirect::route('testes.index');
    }

}