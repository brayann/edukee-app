<?php

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$crawler = $this->client->request('GET', '/');

		$this->assertCount(1, $crawler->filter('h1:contains("Imersao Laravel 4 Edukee")'));

		$this->assertTrue($this->client->getResponse()->isOk());
	}

	public function testProdutoNaoEncontrado() {

		$productRepository = Mockery::mock('ProdutoRepositoryInterface');
		// Cria uma falsa instancia de ProdutoRepositoryInterface

		$productRepository->shouldReceive('find')->once()->andReturn(false);
		// Cria um metodo ficticio chamado 'find' que irá retornar false

		$this->app->instance('ProdutoRepositoryInterface', $productRepository);
		// Faz o app::bind de ProdutoRepositoryInterface para a instancia $productRepository

		$produto = $this->client->request('GET','admin/produto/editar/1');

		$this->assertRedirectedTo('admin/produto');

	}

}