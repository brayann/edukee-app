<?php

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class UserTest extends TestCase {

    public function testNovoUsuario() {

        $user = FactoryMuff::create('User');

        $saved = $user->save();

        $this->assertTrue( $saved );

    }

    /**
     * @expectedException Exception
     */
    public function testNovoUsuarioDeveFalhar() {

        $user = FactoryMuff::create('User');

        $user->nome = null;

        $saved = $user->save();

    }

    public function testRegisterShouldRegister() {

        $user = FactoryMuff::create('User');

        $user->nome         = 'Brayan';
        $user->email        = 'brayan.laurindo@gmail.com';

        $user->save();

        $saved = $user->id ? true : false;

        $this->assertTrue( $saved );

    }

}