<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class EdukeeCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'edukee:produtos';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Trabalha com produtos via CLI';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(User $u)
	{
		
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$this->info('Rodou..');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('id', InputArgument::OPTIONAL, 'ID do Produto'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('list', null, InputOption::VALUE_OPTIONAL, 'Listar produtos', null),
		);
	}

}