<?php

use Illuminate\Support\ServiceProvider;

class EdukeeServiceProvider extends ServiceProvider {
	
	public function register() {

		$this->app['edukee.produtos'] = $this->app->share(function($app)
         {
               // funciona como um IoC container para seu comando,          
               //permitindo que passe as dependencias para instancia-lo
	              return new EdukeeCommand(new User);
               // ou p.e. return new MyCommand(User $user);
        });

        $this->app['edukee.outrocommand'] = $this->app->share(function($app)
         {
               // funciona como um IoC container para seu comando,          
               //permitindo que passe as dependencias para instancia-lo
	              return new OutroCommand();
               // ou p.e. return new MyCommand(User $user);
        });

	      $this->commands(
	                'edukee.produtos', 'edukee.outrocommand'
	      );

	}

}