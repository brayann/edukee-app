<?php

class ProdutoTableSeeder extends Seeder {

    public function run()
    {
    	$dados = array(
    		'nome' => 'PS Vita',
    		'descricao'	=> 'Portatil Da Sony',
    		'valor'	=> 1000
    	);

        $produto = new Produto($dados);
		$produto->save();
    }

}