@extends('layouts.scaffold')

@section('main')

<h1>Show Testis</h1>

<p>{{ link_to_route('testes.index', 'Return to all testes') }}</p>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Teste</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{{ $teste->teste }}}</td>
                    <td>{{ link_to_route('testes.edit', 'Edit', array($teste->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('testes.destroy', $teste->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
        </tr>
    </tbody>
</table>

@stop