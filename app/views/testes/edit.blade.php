@extends('layouts.scaffold')

@section('main')

<h1>Edit Testis</h1>
{{ Form::model($teste, array('method' => 'PATCH', 'route' => array('testes.update', $teste->id))) }}
    <ul>
        <li>
            {{ Form::label('teste', 'Teste:') }}
            {{ Form::text('teste') }}
        </li>

        <li>
            {{ Form::submit('Update', array('class' => 'btn btn-info')) }}
            {{ link_to_route('testes.show', 'Cancel', $teste->id, array('class' => 'btn')) }}
        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop