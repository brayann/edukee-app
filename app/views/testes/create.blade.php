@extends('layouts.scaffold')

@section('main')

<h1>Create Testis</h1>

{{ Form::open(array('route' => 'testes.store')) }}
    <ul>
        <li>
            {{ Form::label('teste', 'Teste:') }}
            {{ Form::text('teste') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn')) }}
        </li>
    </ul>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop


