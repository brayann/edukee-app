@extends('layouts.scaffold')

@section('main')

<h1>All Testes</h1>

<p>{{ link_to_route('testes.create', 'Add new teste') }}</p>

@if ($testes->count())
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Teste</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($testes as $teste)
                <tr>
                    <td>{{{ $teste->teste }}}</td>
                    <td>{{ link_to_route('testes.edit', 'Edit', array($teste->id), array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('testes.destroy', $teste->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    There are no testes
@endif

@stop