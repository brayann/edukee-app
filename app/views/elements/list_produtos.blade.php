<tr id="{{ $produto->id }}">
	<td>{{ $produto->nome }}</td>
	<td>{{ isset($produto->categoria->nome) ? $produto->categoria->nome : '' }}</td>
	<td>{{ $produto->valor }}</td>
	<td>
		@if(Auth::check())
			{{ HTML::link('admin/produto/view/' . $produto->id, 'Visualizar') }}
			{{ HTML::link('admin/produto/editar/' . $produto->id, 'Editar') }}
			{{ HTML::link('admin/produto/excluir/' . $produto->id, 'Excluir') }}
		@else
			{{ HTML::link( '#checkout', 'Comprar', array('data-toggle' => 'modal', 'class' => 'btn btn-success checkout')) }}
		@endif
	</td>
</tr>