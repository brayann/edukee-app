<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="brand" href="#">Laravel Project</a>
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li class="active"><a href="#">Home</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categorias <b class="caret"></b></a>
            <ul class="dropdown-menu">
              @foreach($categorias as $categoria)
                <li>{{ HTML::link('#', $categoria->nome) }}</li>
              @endforeach
            </ul>
          </li>
        </ul>
        <div class="pull-right">
          @if(Auth::check())

            <span>Olá, {{ Auth::User()->nome }}.</span>
            {{ HTML::link('logout', 'Sair') }}

          @else
            {{ Form::open(array('url' => 'login', 'class' => 'navbar-form')) }}
              {{ Form::input('text', 'email', Input::old('email'), array('class' => 'span2')) }}
              {{ Form::input('password', 'password', '', array('class' => 'span2')) }}
              {{ Form::submit('Enviar') }}
            {{ Form::close() }}
          @endif
        </div>
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>