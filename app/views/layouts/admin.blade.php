<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Edukee - Administração</title>
	{{ Basset::show('bootstrapper.css') }}
	{{ Asset::styles() }}
</head>
<body>

	@include('elements.header')

	<div class="container">

		<div class="row-fluid">
			<div class="message">

				@if(Session::has('sucesso'))

					{{ Alert::success(Session::get('sucesso')) }}</div>

				@endif

	        </div>
		</div>
		
		<div class="row-fluid">

			<div class="span3">
				@section('sidebar')
					<div class="well sidebar-nav">
			            <ul class="nav nav-list">
			            	<li class="nav-header">Gerenciar site</li>
			            	<li>
			            		{{ HTML::link('admin/produto', Lang::get('menu.listar')) }}
			            	</li>
			            	<li>
			            		{{ HTML::link('admin/produto/create', Lang::get('menu.cadastrar')) }}
			            	</li>
			            </ul>
			        </div>
		        @show
	        </div>

	        <div class="span8 well">
	        	@yield('conteudo')
	        </div>

	    </div>

	</div>
	{{ Basset::show('bootstrapper.js') }}
	{{ Asset::scripts() }}
</body>
</html>