<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Edukee</title>
	{{ Basset::show('bootstrapper.css') }}
	{{ Asset::styles() }}
</head>
<body>

	@include('elements.header')

	<div>

        <div class="container">
            @yield('conteudo')
        </div>

	</div>
	{{ Basset::show('bootstrapper.js') }}
	{{ Asset::scripts() }}
</body>
</html>