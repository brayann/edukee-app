@section('sidebar')
	@parent
@stop

@section('conteudo')
	<table class="table table-bordered table-striped table-hover">
		<tr>
			<th>Nome</th>
			<th>Categoria</th>
			<th>Preço</th>
			<th>Ações</th>
		</tr>

		@foreach($produtos as $produto) 

			@include('elements.list_produtos')

		@endforeach
	</table>

	<div id="checkout" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel"></h3>
	  </div>

	  	<form name="bcash" action="https://www.bcash.com.br/checkout/pay/" method="post">

			<input name="email_loja" type="hidden" value="brayan.laurindo@gmail.com"> 
			<input name="produto_codigo_1" type="hidden" id="id_produto" value="">

			<input name="produto_descricao_1" id="input_descricao" type="hidden" value="">
			<input name="produto_valor_1" id="input_valor" type="hidden" value="" >
			<input name="tipo_integracao" type="hidden" value="PAD">
			<input name="frete" type="hidden" value="0">

			<div class="modal-body">
				<span id="descricao">Descrição: </span>
				<span id="valor">Valor: </span>
			</div>

			Quantidade: <input name="produto_qtde_1" type="text" value="1" class="input-small"> 

			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
				<input type="image" src="https://www.bcash.com.br/webroot/img/bt_comprar.gif" value="Comprar" alt="Comprar" border="0" align="absbottom" >
			</div>

		</form>

	</div>

@stop