@section('conteudo')

	@if (isset($errors))
		@foreach($errors->all() as $error)

			{{ Alert::error($error) }}

		@endforeach
	@endif

	{{ Form::open() }}

		{{ Form::hidden('id', isset($produto->id) ? $produto->id : '') }}

		<p>
			{{ Form::label('Nome') }}
			{{ Form::text('nome', isset($produto->nome) ? $produto->nome : Input::old('nome'), array('class' => 'input')) }}
			{{ $errors->first('nome') }}
		</p>
		<p>
			{{ Form::label('Valor') }}
			{{ Form::text('valor', isset($produto->valor) ? $produto->valor : Input::old('valor'), array('class' => 'input')) }}
		</p>

		<p>
			{{ Form::label('Categoria') }}
			{{ Form::select('categorias_id', $categorias, isset($produto->categorias_id) ? $produto->categorias_id : Input::old('categorias_id')) }}
		</p>

		<p>
			{{ Form::label('Descrição') }}
			{{ Form::textarea('descricao', (isset($produto->descricao)) ? $produto->descricao : Input::old('descricao')) }}		
		</p>

		<p>
			{{ Form::submit('Enviar') }}
		</p>

	{{ Form::close() }}

@stop