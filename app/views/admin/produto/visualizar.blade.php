@section('conteudo')

	<p><strong>Nome: </strong> {{ $produto->nome }}</p>
	<p><strong>Valor: </strong> {{ $produto->valor }}</p>
	<p><strong>Categoria: </strong> {{ isset($produto->categoria->nome) ? $produto->categoria->nome : '' }}</p>
	<p>
		<strong>Descrição:</strong>
		{{ $produto->descricao }}
	</p>

@stop