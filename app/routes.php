<?php

ViewComposers::register();

// Exemplo de campo personalizado para formularios
Form::macro('fone', function($name)
{
    return '<input type="fone" name="'.$name.'" />';
});

Event::subscribe(new ProdutoEventHandler());

Log::listen(function($level, $message, $context)
{
    //dd($level);
});

// ------------ IoC Container
// 
class Edukee {}
class FooBar {

    public function __construct(Edukee $e)
    {
        $this->edukee = $e;
    }

}

$fooBar = App::make('FooBar');

App::bind('ProdutoRepositoryInterface', 'Produto');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Carregado automaticamente ao chamarmos a view produtos

Route::group(array('prefix' => 'api'), function() {
	Route::resource('produto', 'ProdutoController'); // API de exemplo
});

Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

    Route::controller('produto', 'Admin\ProdutoController'); // Admin, com cadastro e listagem

});

Route::get('mail', function () {

	Mail::send('emails.edukee', array(), function($message)
	{
    		$message->to('brayan.laurindo@gmail.com', 'Brayan Rastelli')->subject('Welcome!');
	});

});

Route::resource('testes', 'TestesController');

Route::controller('/', 'HomeController'); // Pagina inicial, e metodos com exemplos de consulta com Eloquent
